// CRUD Operations
/* 
    C - Create (Insert data)
    R - Read/Retrieve (View specific document)
    U - Update (Edit specific document)
    D - Delete (Delete specific document)

    - CRUD Operations are the heart of any backend application
*/

// [SECTION] Insert documents (create)
/* 
    Syntax:
        db.collectionName.insertOne({object})
*/

db.users.insertOne({
    firstName: "Jane",
    lastName: "Doe",
    age: 21,
    contact: {
        phone: "87654321",
        email: "janedoe@gmail.com"
    },
    courses: ["CSS", "JavaScript", "Python"],
    department: "none"
});

db.rooms.insertOne({
    name: "TJ",
    accomodates: 2,
    price: 1000,
    description: "A simple room with basic necessities",
    roomsAvailable: 10,
    isAvailable: false
});

// Insert many
/* 
    Syntax:
    - db.collectionName.insertMany([{objectA}, {objectB}])
*/

db.users.insertMany([{
    firstName: "Stephen",
    lastName: "Hawking",
    age: 76,
    contact: {
        phone: "87654321",
        email: "stephenhawking@gmail.com"
    },
    courses: ["Python", "React", "PHP"],
    department: "none"
},
{
    firstName: "Neil",
    lastName: "Armstrong",
    age: 82,
    contact: {
        phone: "87654321",
        email: "neilarmstrong@gmail.com"
    },
    courses: ["React", "Laravel", "Sass"],
    department: "none"
}
]);

db.rooms.insertMany([{
    name: "double",
    accomodates: 3,
    price: 2000,
    description: "A room fit for a small family going on a vacation",
    rooms_available: 5,
    isAvailable: false
},
{
    name: "queen",
    accomodates: 4,
    price: 4000,
    description: "A room with a queen sized bed perfect for a simple getaway",
    rooms_available: 15,
    isAvailable: false
}
]);

// [SECTION] Retrieve a document (Read)
/* 
    Syntax:
        - db.collectionName.find({});
        - db.collectionName.find({field:value});
*/
db.getCollection('rooms').find({});
db.users.find({});
db.users.find({firstName: "Stephen"});
db.users.find({department: "none"});
//wild card
db.users.find({firstName: /S/}); // returns a field whose first name starts with S

// Find documents with multiple parameters
/* 
    Syntax:
        - db.collectionName.find({fieldA:valueA}, {fieldB:valueB}); // Returns specific field
        - db.collectionName.find({fieldA:valueA, fieldB:valueB});
*/
db.users.find({lastName:"Armstrong", age:82});

// [SECTION] Updating documents (Update)

// Create a document to update
db.users.insertOne({
    firstName: "Test",
    lastName: "Test",
    age: 0,
    contact: {
        phone: "0000000",
        email: "test@gmail.com"
    },
    course: [],
    department: "none"
});
/* 
    - Just like the find method, methods that only manipulate a single document will only update the FIRST document that matches the search criteria
    Syntax:
        - db.collectionName.updateOne({criteria}, {$set:{field:value}})
*/
db.users.updateOne({firstName:"Test"}, 
{
    $set:{
        firstName: "Bill",
        lastName: "Gates",
        age: 65,
        contact: {
            phone: "12345678",
            email: "bill@gmail.com"
        },
        course: ["PHP", "Laravel", "HTML"],
        department: "Operations",
        status: "active"
    }
});

// Updating multiple documents
/* 
    Syntax:
    - db.collectionName.updateMany({criteria}, {field:value})
*/

db.users.updateMany({department:"none"}, 
{
    $set:{
       department: "HR"
    }
});

db.users.updateOne({firstName:"Bill"},
{
    $set:{
        phone: "87654321"
    }
});

// Replace One
/* 
    - Can be used if replacing the whole document if necessary
    Syntax:
         - db.coollectionName.replaceOne({criteria}, {$set: {field:value}});
*/
db.users.replaceOne({firstName:"Bill"},
{
        firstName: "Bill",
        lastName: "Gates",
        age: 65,
        contact: {
            phone: "12345678",
            email: "bill@gmail.com"
        },
        courses: ["PHP", "Laravel", "HTML"],
        department: "Operations",
});


db.rooms.updateOne({name:"queen"},
{
    $set:{
        rooms_available: 0
    }
});
db.rooms.find({name:"queen"});

// [SECTION] Removing documents [DELETE]

// Deleting a single document
/* 
    Syntax:
        - db.collectionName.deleteOne({criteria});
*/

db.users.deleteOne({
    firstName: "Test"
});
db.users.find();

// Delete Many
/* 
    - Be careful when using "deleteMany" method. If no search criteria is provided, it will delete all documents in the collection
    Syntax:
        - db.collectionName.deleteMany({criteria});
        - db.collectionName.deleteOne({}); // DO NOT USE
*/
db.users.deleteMany({
    firstName: "Test"
});

db.rooms.deleteMany({
    rooms_available: 0
});

// [SECTION] Advanced Queries
/* 
    
*/

db.users.find({
    contact:{
        phone: "87654321",
        email: "stephenhawking@gmail.com"
        }
});

// Query on nested field
db.users.find({
    "contact.email":"stephenhawking@gmail.com"
});

db.users.find({
    "contact.email":/steph/
});

// Querying an array with Exact Element
db.users.find(
    {
    courses: ["CSS", "JavaScript", "Python"]
}
);

// Querying an array disregarding the array elements order
// $all matches documents where the field contains the nested array elements
db.users.find(
    {
    courses: {$all: ["JavaScript", "CSS", "Python"]}
}
);

// will retrieve all documents containing the "Python" as element of the nested array
db.users.find(
    {
    courses: {$all: ["Python"]}
}
);

db.users.find(
    {
    courses: /Python/
}
);

// Querying an Embedded Array
db.users.insertOne({
    nameArr:[
        {
            nameA: "Juan"
        },
        {
            nameB: "Tamad"
        }
    ]
});

db.users.find({
    nameArr:{
        nameA: "Juan"
    }
});




